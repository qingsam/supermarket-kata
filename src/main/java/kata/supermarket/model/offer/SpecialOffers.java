package kata.supermarket.model.offer;

import kata.supermarket.model.product.Product;

import java.util.HashMap;
import java.util.Map;

public class SpecialOffers {
    private Map<Product, Offer> offers;

    public SpecialOffers(Map<Product, Offer> offers) {
        this.offers = offers;
    }

    public SpecialOffers() {
        this.offers = new HashMap<>();
    }

    public void add(Product product, Offer offer) {
        this.offers.put(product, offer);
    }

    public Map<Product, Offer> getOffers() {
        return offers;
    }
}
