package kata.supermarket.adaptor;

import com.sun.tools.javac.util.List;
import kata.supermarket.model.item.Item;
import kata.supermarket.model.offer.Offer;
import kata.supermarket.model.offer.OfferByUnit;
import kata.supermarket.model.offer.OfferByWeight;
import kata.supermarket.model.product.UnitProduct;
import kata.supermarket.model.product.WeighedProduct;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DiscountAdaptorTest {

    @Test
    public void testGetWithUnitProduct() {
        UnitProduct product = new UnitProduct("", BigDecimal.valueOf(0.5));
        OfferByUnit offer = new OfferByUnit(BigInteger.valueOf(2), BigDecimal.valueOf(0.6));
        List<Item> itemList =  List.of(product.oneOf(), product.oneOf());
        BigDecimal actual = DiscountAdaptor.get(product)
                .apply(itemList, Optional.of(offer));
        BigDecimal expected = BigDecimal.valueOf(0.4);
        assertEquals(expected, actual);
    }

    @Test
    public void testGetWithUnitProductAndNoItem() {
        // TODO
    }

    @Test
    public void testGetWithWeightedProduct() {
        WeighedProduct product = new WeighedProduct("", BigDecimal.valueOf(1.0));
        OfferByWeight offer = new OfferByWeight(BigDecimal.valueOf(2.0), BigDecimal.valueOf(0.6));
        List<Item> itemList =  List.of(product.weighing(BigDecimal.valueOf(1.0)), product.weighing(BigDecimal.valueOf(1.0)));
        
        // TODO fix scale
        BigDecimal actual = DiscountAdaptor.get(product)
                .apply(itemList, Optional.of(offer)).setScale(2, RoundingMode.CEILING);
        BigDecimal expected = BigDecimal.valueOf(1.4).setScale(2, RoundingMode.CEILING);  // 2.0 - 0.6 = 1.4
        assertEquals(expected, actual);
    }

    @Test
    public void testGetWithWeightedProductAndNoItem() {
        // TODO
    }
}
