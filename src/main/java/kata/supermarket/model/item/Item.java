package kata.supermarket.model.item;

import kata.supermarket.model.product.Product;

import java.math.BigDecimal;

public interface Item<E extends Product> {
    BigDecimal price();
    E product();
}
