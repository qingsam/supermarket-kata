package kata.supermarket.model.item;

import kata.supermarket.model.product.UnitProduct;

import java.math.BigDecimal;

public class ItemByUnit implements Item<UnitProduct> {

    private final UnitProduct unitProduct;

    public ItemByUnit(final UnitProduct unitProduct) {
        this.unitProduct = unitProduct;
    }

    public BigDecimal price() {
        return unitProduct.pricePerUnit();
    }

    @Override
    public UnitProduct product() {
        return unitProduct;
    }
}
