package kata.supermarket.model.offer;

import java.math.BigDecimal;

/**
 * OfferByWeight class contains the requirement for the offer and at what price.
 * This will allow us to specify any offer we want with the use of a single class
 */
public class OfferByWeight implements Offer{
    final BigDecimal weightInKilos;
    final BigDecimal priceAt;

    public OfferByWeight(BigDecimal weightInKilos, BigDecimal priceAt) {
        this.weightInKilos = weightInKilos;
        this.priceAt = priceAt;
    }

    public BigDecimal getWeightInKilos() {
        return weightInKilos;
    }

    public BigDecimal getPriceAt() {
        return priceAt;
    }
}
