package kata.supermarket.model.product;

import kata.supermarket.model.item.Item;
import kata.supermarket.model.item.ItemByUnit;

import java.math.BigDecimal;
import java.util.Objects;

public class UnitProduct implements Product {

    private final String sku;
    private final BigDecimal pricePerUnit;

    public UnitProduct(final String sku, final BigDecimal pricePerUnit) {
        this.sku = sku;
        this.pricePerUnit = pricePerUnit;
    }

    public BigDecimal pricePerUnit() {
        return pricePerUnit;
    }

    public Item oneOf() {
        return new ItemByUnit(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UnitProduct unitProduct = (UnitProduct) o;
        return Objects.equals(sku, unitProduct.sku);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sku);
    }
}
