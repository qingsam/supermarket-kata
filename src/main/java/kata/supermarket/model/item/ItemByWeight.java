package kata.supermarket.model.item;

import kata.supermarket.model.product.WeighedProduct;

import java.math.BigDecimal;

public class ItemByWeight implements Item<WeighedProduct> {

    private final WeighedProduct product;
    private final BigDecimal weightInKilos;

    public ItemByWeight(final WeighedProduct product, final BigDecimal weightInKilos) {
        this.product = product;
        this.weightInKilos = weightInKilos;
    }

    public BigDecimal price() {
        return product.pricePerKilo().multiply(weightInKilos).setScale(2, BigDecimal.ROUND_HALF_UP);
    }

    @Override
    public WeighedProduct product() {
        return product;
    }

    public BigDecimal getWeightInKilos() {
        return weightInKilos;
    }

}
