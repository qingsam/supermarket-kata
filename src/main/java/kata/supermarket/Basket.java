package kata.supermarket;

import kata.supermarket.adaptor.DiscountAdaptor;
import kata.supermarket.model.item.Item;
import kata.supermarket.model.offer.SpecialOffers;
import kata.supermarket.model.product.Product;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.stream.Collectors;

public class Basket {
    private final List<Item> items;

    public Basket() {
        this.items = new ArrayList<>();
    }

    public void add(final Item item) {
        this.items.add(item);
    }

    List<Item> items() {
        return Collections.unmodifiableList(items);
    }

    public BigDecimal total(Optional<SpecialOffers> specialOffers) {
        return specialOffers
                .map(so -> new TotalCalculator().calculate(so))
                .orElse(new TotalCalculator().calculate());
    }

    private class TotalCalculator {
        private final List<Item> items;

        TotalCalculator() {
            this.items = items();
        }

        private BigDecimal subtotal() {
            return items.stream().map(Item::price)
                    .reduce(BigDecimal::add)
                    .orElse(BigDecimal.ZERO)
                    .setScale(2, RoundingMode.HALF_UP);
        }

        /**
         * Calc the discounts based on the Special offers passed.
         * The function uses the DiscountAdaptor to select the correct function to apply for
         * the product.
         * @param specialOffers
         */
        private BigDecimal discounts(SpecialOffers specialOffers) {
            Map<Product, List<Item>> itemsMap = items.stream()
                    .collect(Collectors.groupingBy(item -> item.product())); // TODO use some id as key instead?

            return itemsMap.keySet().stream() // Stream each group of products
                    .map(productImp ->
                            // apply discount function to each list of x product
                            DiscountAdaptor.get(productImp).apply(
                                    itemsMap.get(productImp),
                                    Optional.ofNullable(specialOffers.getOffers().get(productImp))))
                    .reduce(BigDecimal::add)
                    .orElse(BigDecimal.ZERO)
                    .setScale(2, RoundingMode.HALF_UP); // TODO move rounding
        }

        private BigDecimal calculate() {
            return subtotal();
        }

        private BigDecimal calculate(SpecialOffers specialOffers) {
            return subtotal().subtract(discounts(specialOffers));
        }
    }
}