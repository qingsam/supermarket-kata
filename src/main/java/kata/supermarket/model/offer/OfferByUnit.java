package kata.supermarket.model.offer;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Objects;

/**
 * OfferByUnit class contains the requirement for the offer and at what price.
 * This will allow us to specify any offer we want with the use of a single class
 */
public class OfferByUnit implements Offer {
    final BigInteger quantity; // TODO rename reqQuantity
    final BigDecimal priceAt;

    public OfferByUnit(BigInteger quantity, BigDecimal priceAt) {
        this.quantity = quantity;
        this.priceAt = priceAt;
    }

    public BigInteger getQuantity() {
        return quantity;
    }

    public BigDecimal getPriceAt() {
        return priceAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OfferByUnit that = (OfferByUnit) o;
        return Objects.equals(quantity, that.quantity) && Objects.equals(priceAt, that.priceAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(quantity, priceAt);
    }
}
