package kata.supermarket.model.product;

import kata.supermarket.model.item.Item;
import kata.supermarket.model.item.ItemByWeight;

import java.math.BigDecimal;
import java.util.Objects;

public class WeighedProduct implements Product{

    private final String sku;
    private final BigDecimal pricePerKilo;

    public WeighedProduct(final String sku, final BigDecimal pricePerKilo) {
        this.sku = sku;
        this.pricePerKilo = pricePerKilo;
    }

    public BigDecimal pricePerKilo() {
        return pricePerKilo;
    }

    public Item weighing(final BigDecimal kilos) {
        return new ItemByWeight(this, kilos);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WeighedProduct that = (WeighedProduct) o;
        return Objects.equals(sku, that.sku);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sku);
    }
}
