package kata.supermarket.model.offer;

import kata.supermarket.model.product.Product;
import kata.supermarket.model.product.UnitProduct;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SpecialOffersTest {

    @Test
    public void addOfferToSpecialOffers() {
        SpecialOffers specialOffers = new SpecialOffers();
        UnitProduct product = new UnitProduct("some_product", BigDecimal.valueOf(10.0));
        specialOffers.add(new UnitProduct("some_product", BigDecimal.valueOf(10.0)), new OfferByUnit(BigInteger.valueOf(2), BigDecimal.valueOf(15.0)));
        Map<Product, Offer> actualOffers =  specialOffers.getOffers();

        Offer actualOffer = actualOffers.get(product);
        Offer expected = new OfferByUnit(BigInteger.valueOf(2), BigDecimal.valueOf(15.0));
        assertEquals(expected, actualOffer);
    }
}
