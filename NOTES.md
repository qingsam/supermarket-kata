# Notes

Please add here any notes, assumptions and design decisions that might help up understand your though process.
- I have added a Sku in UnitProduct and WeighedProduct classes. This will be a unique id and will get used to get the Offers from the SpecialOffers Class.
- I have created an OfferByUnit and OfferByWeight. Both will accept two param, (1) is the quanity required for the offer, (2) the price for the offer. This will allow us to create and name any offer we like without much restrictions.
- I have added a discount Adaptor class. This will return a function to calc the discount. In this case, function to calc unit and weighed products. Can be expandable if required.

TODO
- A tests is failing because of some rounding error. We might need to round the price based on the final calculated results
- I have not yet added much tests for the classes I've created in the model and adaptor pacakge.
- Test for negative scenarios bad scenarios.
- Factory pattern to create products
