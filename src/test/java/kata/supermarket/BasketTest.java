package kata.supermarket;

import kata.supermarket.model.item.Item;
import kata.supermarket.model.offer.OfferByUnit;
import kata.supermarket.model.offer.OfferByWeight;
import kata.supermarket.model.offer.SpecialOffers;
import kata.supermarket.model.product.UnitProduct;
import kata.supermarket.model.product.WeighedProduct;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BasketTest {

    @DisplayName("basket provides its total value when containing...")
    @MethodSource
    @ParameterizedTest(name = "{0}")
    void basketProvidesTotalValue(String description, String expectedTotal, Iterable<Item> items) {
        final Basket basket = new Basket();
        items.forEach(basket::add);
        assertEquals(new BigDecimal(expectedTotal), basket.total(Optional.empty()));
    }

    static Stream<Arguments> basketProvidesTotalValue() {
        return Stream.of(
                noItems(),
                aSingleItemPricedPerUnit(),
                multipleItemsPricedPerUnit(),
                aSingleItemPricedByWeight(),
                multipleItemsPricedByWeight()
        );
    }

    @DisplayName("basket provides its total with discount value when containing...")
    @MethodSource
    @ParameterizedTest(name = "{0}")
    void basketProvidesTotalValueWithSpecialOffers(String description, String expectedTotal, Iterable<Item> items) {
        SpecialOffers specialOffers = new SpecialOffers();
        specialOffers.add(aPackOfDigestives().product(), new OfferByUnit(BigInteger.valueOf(2), BigDecimal.valueOf(2.0)));
        specialOffers.add(aPintOfMilk().product(), new OfferByUnit(BigInteger.valueOf(2), BigDecimal.valueOf(0.5)));
        specialOffers.add(twoFiftyGramsOfAmericanSweets().product(), new OfferByWeight(BigDecimal.valueOf(0.5), BigDecimal.valueOf(0.5)));

        final Basket basket = new Basket();
        items.forEach(basket::add);
        assertEquals(new BigDecimal(expectedTotal), basket.total(Optional.of(specialOffers)));
    }

    static Stream<Arguments> basketProvidesTotalValueWithSpecialOffers() {
        return Stream.of(
                Arguments.of("Discount", "2.54",
                        Arrays.asList(aPackOfDigestives(), aPintOfMilk(), aPintOfMilk(), aPintOfMilk())),
                Arguments.of("Discount", "1.00",
                        Arrays.asList(twoFiftyGramsOfAmericanSweets(), twoFiftyGramsOfAmericanSweets(), twoFiftyGramsOfAmericanSweets(), twoFiftyGramsOfAmericanSweets()))
        );
    }
    private static Arguments aSingleItemPricedByWeight() {
        return Arguments.of("a single weighed item", "1.25", Collections.singleton(twoFiftyGramsOfAmericanSweets()));
    }

    private static Arguments multipleItemsPricedByWeight() {
        return Arguments.of("multiple weighed items", "1.85",
                Arrays.asList(twoFiftyGramsOfAmericanSweets(), twoHundredGramsOfPickAndMix())
        );
    }

    private static Arguments multipleItemsPricedPerUnit() {
        return Arguments.of("multiple items priced per unit", "2.04",
                Arrays.asList(aPackOfDigestives(), aPintOfMilk()));
    }

    private static Arguments aSingleItemPricedPerUnit() {
        return Arguments.of("a single item priced per unit", "0.49", Collections.singleton(aPintOfMilk()));
    }

    private static Arguments noItems() {
        return Arguments.of("no items", "0.00", Collections.emptyList());
    }

    private static Item aPintOfMilk() {
        return new UnitProduct("Milk", new BigDecimal("0.49")).oneOf();
    }

    private static Item aPackOfDigestives() {
        return new UnitProduct("Digestives", new BigDecimal("1.55")).oneOf();
    }

    private static WeighedProduct aKiloOfAmericanSweets() {
        return new WeighedProduct("AmericanSweets", new BigDecimal("4.99"));
    }

    private static Item twoFiftyGramsOfAmericanSweets() {
        return aKiloOfAmericanSweets().weighing(new BigDecimal(".25"));
    }

    private static WeighedProduct aKiloOfPickAndMix() {
        return new WeighedProduct("PickAndMix", new BigDecimal("2.99"));
    }

    private static Item twoHundredGramsOfPickAndMix() {
        return aKiloOfPickAndMix().weighing(new BigDecimal(".2"));
    }
}