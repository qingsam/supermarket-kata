package kata.supermarket.adaptor;

import kata.supermarket.model.item.Item;
import kata.supermarket.model.item.ItemByWeight;
import kata.supermarket.model.offer.Offer;
import kata.supermarket.model.offer.OfferByUnit;
import kata.supermarket.model.offer.OfferByWeight;
import kata.supermarket.model.product.Product;
import kata.supermarket.model.product.UnitProduct;
import kata.supermarket.model.product.WeighedProduct;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;

public class DiscountAdaptor {

    public static BiFunction<List<Item>, Optional<? extends Offer>, BigDecimal> get(Product product) {
        if (product instanceof UnitProduct) {
            return unitDiscountFunc;
        } else if (product instanceof WeighedProduct) {
            return weighedDiscountFunc;
        } else {
            throw new IllegalArgumentException("Invalid ProductImp class. Cannot get Discount function");
        }
    }

    private static BiFunction<List<Item>, Optional<? extends Offer>, BigDecimal> unitDiscountFunc = (items, offer) ->
            ((Optional<OfferByUnit>) offer).map(o -> {
                BigInteger offerQuantity = o.getQuantity();
                BigDecimal offerPrice = o.getPriceAt();

                // get number of offers met for current list of items
                int numOfOffer = items.size() / offerQuantity.intValue();

                BigDecimal originalPrice = items.get(0).price();
                BigInteger quantity = offerQuantity.multiply(BigInteger.valueOf(numOfOffer)); // quantity of item with offer

                BigDecimal priceBeforeDiscount = originalPrice.multiply(new BigDecimal(quantity));
                BigDecimal priceAfterDiscount = offerPrice.multiply(BigDecimal.valueOf(numOfOffer));
                return priceBeforeDiscount.subtract(priceAfterDiscount);
            }).orElse(BigDecimal.ZERO);

    private static BiFunction<List<Item>, Optional<? extends Offer>, BigDecimal> weighedDiscountFunc = (items, offer) ->
            ((Optional<OfferByWeight>) offer).map(o -> {
                BigDecimal offerWeightInKilos = o.getWeightInKilos();
                BigDecimal offerPrice = o.getPriceAt();

                BigDecimal netWeight = items.stream()
                        .map(ItemByWeight.class::cast)
                        .map(ItemByWeight::getWeightInKilos)
                        .reduce(BigDecimal::add)
                        .orElse(BigDecimal.ZERO);

                // get number of offers met for current list of items
                int numOfOffer = netWeight.divide(offerWeightInKilos).intValue();

                WeighedProduct product = (WeighedProduct) items.get(0).product();

                BigDecimal priceBeforeDiscount = product.weighing(netWeight).price();
                BigDecimal priceAfterDiscount = offerPrice.multiply(BigDecimal.valueOf(numOfOffer));
                return priceBeforeDiscount.subtract(priceAfterDiscount);
            }).orElse(BigDecimal.ZERO);

}
